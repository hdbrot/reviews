-- Your database schema. Use the Schema Designer at http://localhost:8001/ to add some tables.
CREATE TABLE books (
    id UUID DEFAULT uuid_generate_v4() PRIMARY KEY NOT NULL,
    title TEXT NOT NULL UNIQUE,
    author TEXT DEFAULT NULL,
    content TEXT DEFAULT NULL
);
CREATE TABLE reviews (
    id UUID DEFAULT uuid_generate_v4() PRIMARY KEY NOT NULL,
    book_id UUID NOT NULL,
    user_id UUID NOT NULL,
    created_at TIMESTAMP WITH TIME ZONE DEFAULT NOW() NOT NULL,
    edited_at TIMESTAMP WITH TIME ZONE DEFAULT NULL,
    title TEXT NOT NULL,
    rating INT NOT NULL,
    body TEXT DEFAULT NULL,
    score INT DEFAULT 0 NOT NULL
);
CREATE INDEX reviews_book_id_index ON reviews (book_id);
CREATE TABLE users (
    id UUID DEFAULT uuid_generate_v4() PRIMARY KEY NOT NULL,
    email TEXT NOT NULL UNIQUE,
    password_hash TEXT NOT NULL,
    locked_at TIMESTAMP WITH TIME ZONE DEFAULT NULL,
    failed_login_attempts INT DEFAULT 0 NOT NULL
);
CREATE INDEX reviews_user_id_index ON reviews (user_id);
CREATE TABLE votes (
    id UUID DEFAULT uuid_generate_v4() PRIMARY KEY NOT NULL,
    user_id UUID NOT NULL,
    review_id UUID NOT NULL
);
CREATE INDEX values_user_id_index ON votes (user_id);
CREATE INDEX values_review_id_index ON votes (review_id);
ALTER TABLE reviews ADD CONSTRAINT reviews_ref_book_id FOREIGN KEY (book_id) REFERENCES books (id) ON DELETE NO ACTION;
ALTER TABLE reviews ADD CONSTRAINT reviews_ref_user_id FOREIGN KEY (user_id) REFERENCES users (id) ON DELETE NO ACTION;
ALTER TABLE votes ADD CONSTRAINT votes_ref_review_id FOREIGN KEY (review_id) REFERENCES reviews (id) ON DELETE NO ACTION;
ALTER TABLE votes ADD CONSTRAINT votes_ref_user_id FOREIGN KEY (user_id) REFERENCES users (id) ON DELETE NO ACTION;
