module Application.Helper.Controller where

import IHP.ControllerPrelude

import qualified Data.Text as Text (all)
import qualified Data.Char as Char (isSpace)

-- Validators

validateMoreThanWhiteSpace :: Text -> ValidatorResult
validateMoreThanWhiteSpace text =
  if isWhiteSpace text
  then Failure "This field must be provided."
  else Success

isWhiteSpace = Text.all Char.isSpace

whiteSpaceToNothing field =
  modify field (maybe Nothing stripWhiteSpace)

stripWhiteSpace = \text ->
  if isWhiteSpace text
  then Nothing
  else Just text

-- Here you can add functions which are available in all your controllers
