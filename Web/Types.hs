module Web.Types where

import IHP.Prelude
import IHP.ModelSupport
import IHP.LoginSupport.Types
import Generated.Types


data WebApplication = WebApplication deriving (Eq, Show)

data StaticController = WelcomeAction deriving (Eq, Show, Data)

data RatingOrder = FiveToOne | OneToFive
  deriving (Eq, Show, Data)
data EditOrder = EarlyToLate | LateToEarly
  deriving (Eq, Show, Data)

instance HasNewSessionUrl User where
  newSessionUrl _ = "/"

type instance CurrentUserRecord = User

data SessionsController
  = NewSessionAction
  | CreateSessionAction
  | DeleteSessionAction
  deriving (Eq, Show, Data)

data BooksController
  = BooksAction
  | NewBookAction
  | ShowBookAction { bookId :: !(Id Book) }
  | CreateBookAction
  | EditBookAction { bookId :: !(Id Book) }
  | UpdateBookAction { bookId :: !(Id Book) }
  | DeleteBookAction { bookId :: !(Id Book) }
  deriving (Eq, Show, Data)

data ReviewsController
--  ReviewsAction
  = NewReviewAction { bookId :: !(Id Book) }
--  | ShowReviewAction { reviewId :: !(Id Review) }
  | CreateReviewAction
  | EditReviewAction { reviewId :: !(Id Review) }
  | UpdateReviewAction { reviewId :: !(Id Review) }
  | DeleteReviewAction { reviewId :: !(Id Review) }
  | LikeReviewAction { reviewId :: !(Id Review) }
  deriving (Eq, Show, Data)

data UsersController
  = --UsersAction
    NewUserAction
--  | ShowUserAction { userId :: !(Id User) }
  | CreateUserAction
  | DeleteUserAction { userId :: !(Id User) }
  deriving (Eq, Show, Data)
