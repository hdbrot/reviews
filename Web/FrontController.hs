module Web.FrontController where

import IHP.RouterPrelude
import IHP.LoginSupport.Middleware
import Web.View.Layout (defaultLayout)

-- Controller Imports
import Web.Controller.Prelude
import Web.Controller.Static
import Web.Controller.Sessions
import Web.Controller.Users
import Web.Controller.Books
import Web.Controller.Reviews

instance FrontController WebApplication where
    controllers = 
        [ startPage BooksAction
        , parseRoute @SessionsController
        -- Generator Marker
        , parseRoute @UsersController
        , parseRoute @ReviewsController
        , parseRoute @BooksController
        ]

instance InitControllerContext WebApplication where
    initContext = do
        setLayout defaultLayout
        initAutoRefresh
        initAuthentication @User
