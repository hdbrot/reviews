module Web.View.Books.Index where

import Web.View.Prelude

data IndexView = IndexView { books :: [Include "reviews" Book] }

instance View IndexView where
  html IndexView { .. } =
    [hsx|
        <div>
         {breadcrumb}
         {loggingActions currentUserOrNothing}
        </div>

        <h1>Books<a href={pathTo NewBookAction} class="btn btn-primary ms-4">+ New</a></h1>
        <div class="table-responsive">
         <table class="table">
          <thead>
           <tr>
            <th>Title</th>
            <th style="text-align:center">Rating</th>
           </tr>
          </thead>
          <tbody>
           {forEach books renderBook}
          </tbody>
         </table>
        </div>
        |]
        where
            breadcrumb = renderBreadcrumb [breadcrumbText "Index"]

renderBook book =
  [hsx|
      <tr>
       <td><a href={pathTo (ShowBookAction book.id)} class="text-muted">
         {book.title}
       </a></td>
       <td style="text-align:center"><h4>
         {renderRating (avgRating book.reviews)}
       </h4></td>
      </tr>
  |]
