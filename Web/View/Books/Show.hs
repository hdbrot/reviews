module Web.View.Books.Show where

import Web.View.Prelude

import IHP.ControllerSupport (request)
import Web.View.Books.Helper (renderBookAuthor, renderBookContent)
import Web.View.Reviews.Helper (newReviewOf, renderReviewForm, renderReview)

data ShowView = ShowView { book :: Include "reviews" Book
--                         , currentReview :: Review
                         }

instance View ShowView where
  html ShowView { .. } =
    [hsx|
        <div>
         {breadcrumb}
         {loggingActions currentUserOrNothing}
        </div>
        <br />
        <div style="display:flex; justify-content:space-between">
         <h1 style="display:flex; align-items:center">{book.title}</h1>
         <div style="display:flex; column-gap:17px">
          <div>
           <a href={pathTo (DeleteBookAction book.id)} class="js-delete text-muted" style="float:right">Delete</a>
           <br />
           <a href={pathTo (EditBookAction book.id)} class="text-muted" style="float:right">Edit</a>
          </div>
          <h4 style="display:flex; align-items:center">{renderRating (avgRating book.reviews)}</h4>
         </div>
        </div>
        <p>{renderBookAuthor book}</p>
        <p>{renderBookContent book}</p>
        <div style="display:flex; justify-content:space-between">
         <h2 style="display:flex; align-items:center">Reviews:
          <a href={pathTo (NewReviewAction book.id)} class="btn btn-primary ms-4">+ New</a>
         </h2>
         <div>
          <a href={pathTo (ShowBookAction book.id)} style="float:right">Helpful on top</a>
          <br />
          <a href={(pathTo (ShowBookAction book.id) <> "&sort=asc")} style="float:right">🞁</a>
          <a href={(pathTo (ShowBookAction book.id) <> "&sort=dsc")} style="float:right">🞃</a>
         </div>
        </div>
        <div class="reviews">
         {forEach book.reviews renderReview}
        </div>
        <br />
    |]
      where
        breadcrumb = renderBreadcrumb
                     [ breadcrumbLink "Back to Index" BooksAction
                     , breadcrumbText "Showing Book"
                     ]
