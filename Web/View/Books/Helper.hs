module Web.View.Books.Helper where

import Web.View.Prelude

renderBookForm :: Book -> Html
renderBookForm book = formFor book
  [hsx|
      {(textField #title)}
      {(textField #author)}
      {(textareaField #content)}
      {submitButton}
  |]

renderBookAuthor :: Include "reviews" Book -> Html
renderBookAuthor book =
  [hsx|
      <b>Author:</b> <br />
  |]
  <>
  case book.author of
    Nothing -> [hsx|No author specified.|]
    Just author -> [hsx|{book.author}|]

renderBookContent :: Include "reviews" Book -> Html
renderBookContent book =
  [hsx|
      <b>Content:</b> <br />
  |]
  <>
  case book.content of
    Nothing -> [hsx|No content description.|]
    Just content -> [hsx|{book.content}|]
