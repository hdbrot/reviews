module Web.View.Books.Edit where

import Web.View.Prelude

import Web.View.Books.Helper (renderBookForm)

data EditView = EditView { book :: Book }

instance View EditView where
  html EditView { .. } =
    renderModal Modal { modalTitle = "Edit Book"
                      , modalCloseUrl = pathTo ShowBookAction { bookId = book.id }
                      , modalFooter = Nothing
                      , modalContent =
                        [hsx| {renderBookForm book} |]
                      }
