module Web.View.Books.New where

import Web.View.Prelude

import Web.View.Books.Helper (renderBookForm)

data NewView = NewView { book :: Book }

instance View NewView where
  html NewView { .. } =
    renderModal Modal { modalTitle = "New Book"
                      , modalCloseUrl = pathTo BooksAction
                      , modalFooter = Nothing
                      , modalContent = [hsx| {renderBookForm book} |]
                      }
