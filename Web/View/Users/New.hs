module Web.View.Users.New where

import Web.View.Prelude

data NewView = NewView { user :: User }
data NewView' = NewView' { user :: User
                         , showUser :: Text
                         }

instance View NewView where
  html NewView { .. } =
    renderModal Modal { modalTitle = "New User"
                      , modalCloseUrl = pathTo BooksAction
                      , modalFooter = Nothing
                      , modalContent = [hsx| {renderUserForm user} |]
                      }

instance View NewView' where
  html NewView' { .. } =
    renderModal Modal { modalTitle = "New User"
                      , modalCloseUrl = pathTo BooksAction
                      , modalFooter = Nothing
                      , modalContent = [hsx|
                                           {showUser}
                                           {renderUserForm user}
                                       |]
                      }

renderUserForm :: User -> Html
renderUserForm user = formFor user
  [hsx|
      {(textField #email)}
      {(passwordField #passwordHash) {fieldLabel = "Password"}}
      {(passwordField #passwordHash) { fieldLabel = "Password confirmation", fieldName = "passwordConfirmation"}}
      {submitButton}
  |]
