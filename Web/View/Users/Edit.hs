module Web.View.Users.Edit where

import Web.View.Prelude

data EditView = EditView { user :: User }

instance View EditView where
  html EditView { .. } =
    renderModal Modal { modalTitle = "Edit User"
                      , modalCloseUrl = pathTo BooksAction
                      , modalFooter = Nothing
                      , modalContent = [hsx| {renderUserForm user} |]
                      }

renderUserForm' :: User -> Text -> Html
renderUserForm' user oldPasswordHash = formFor user
  [hsx|
      {(textField #email)}
      {(passwordField #passwordHash) {fieldLabel = "Password", placeholder = "tt"}}
      {(passwordField #passwordHash) { fieldLabel = "Password confirmation", fieldName = "passwordConfirmation", validatorResult = Nothing }}
      {submitButton}
  |]

renderUserForm :: User -> Html
renderUserForm user = formFor user
  [hsx|
      {(textField #email)}
      {(passwordField #passwordHash) {fieldLabel = "Old Password", fieldName = "oldPassword", validatorResult = Nothing}}
      {(passwordField #passwordHash) {fieldLabel = "New Password", fieldName = "newPassword", validatorResult = Nothing}}
      {(passwordField #passwordHash) {fieldLabel = "Confirm New Password", fieldName = "newPasswordConfirm"}}
      {submitButton}
  |]
