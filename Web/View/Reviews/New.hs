module Web.View.Reviews.New where

import Web.View.Prelude

import Web.View.Reviews.Helper (newReviewOf, renderReviewForm)

data NewView = NewView { bookId :: Id Book
                       , maybeReview :: Maybe Review
                       }

instance View NewView where
  html NewView { .. } = renderModal Modal
    { modalTitle = "New Review"
    , modalCloseUrl = pathTo ShowBookAction { bookId = bookId }
    , modalFooter = Nothing
    , modalContent = [hsx|{renderReviewForm' bookId maybeReview}|]
    }
    where
      renderReviewForm' bookId (Just review) = renderReviewForm review
      renderReviewForm' bookId Nothing = renderReviewForm (newReviewOf bookId)
