module Web.View.Reviews.Helper
    ( newReviewOf
    , renderReviewForm
    , renderReview
    ) where

import Web.View.Prelude


newReviewOf bookId =
  newRecord @Review |> set #bookId bookId

renderReviewForm :: Review -> Html
renderReviewForm review = formFor review
  [hsx|
      {hiddenField #bookId}
      {hiddenField #userId}
      {textField #title}
      {radioField #rating possibleRatings}
      {getValidationFailure #rating review}
      {textareaField #body}
      {submitButton}
  |]
    where
      possibleRatings :: [Rating Int]
      possibleRatings = [ Rating 1
                        , Rating 2
                        , Rating 3
                        , Rating 4
                        , Rating 5
                        ]

data Rating t = Rating t

instance CanSelect (Rating Int) where
  type SelectValue (Rating Int) = Int
  selectValue (Rating n) = n
  selectLabel (Rating n) = show n
{-
ratingField review = fieldset $ do
    input ! A.type_ "radio" ! A.name "rating" ! A.id "1" ! A.value "1" !? (review.rating == 1, checked "true")
    input ! A.type_ "radio" ! A.name "rating" ! A.id "2" ! A.value "2" !? (review.rating == 2, checked "true")
    input ! A.type_ "radio" ! A.name "rating" ! A.id "3" ! A.value "3" !? (review.rating == 3, checked "true")
    input ! A.type_ "radio" ! A.name "rating" ! A.id "4" ! A.value "4" !? (review.rating == 4, checked "true")
    input ! A.type_ "radio" ! A.name "rating" ! A.id "5" ! A.value "5" !? (review.rating == 5, checked "true")
-}
renderReview :: Review -> Html
renderReview review =
  [hsx|
      <hr />
      <div style="display:flex; justify-content:space-between">
       <h3 style="display:flex; align-items:center">{review.title}</h3>
       <div style="display:flex; column-gap:17px">
        <div>
         {when (isCreatorOf review currentUserOrNothing) renderMessageActions}
         </div>
        <h4 style="display:flex; align-items:center">{renderRating review.rating}</h4>
       </div>
      </div>
      <p>{review.body}</p>
      <div style="display:flex; justify-content:space-between">
       <div style="display:flex">
        {creator}
       </div>
       <div style="display:flex; column-gap:17px">
        {editDate}
        {creationDate}
       </div>
      </div>
      Likes: {review.score}
      {when (not $ isCreatorOf review currentUserOrNothing) renderLikeButton}
  |]
    where
        renderMessageActions =
          [hsx|
              <div style="display:flex; column-gap:17px">
               <a href={pathTo (DeleteReviewAction review.id)} class="js-delete text-muted" style="float:right">Delete</a>
               <a href={pathTo (EditReviewAction review.id)} class="text-muted" style="float:right">Edit</a>
              </div>
          |]
        creationDate = [hsx|<span class="text-muted">Created at {dateTime review.createdAt}</span>|]
        creator = [hsx|<span class="text-muted">by {review.userId}</span>|]
        editDate = case review.editedAt of
          Nothing   -> [hsx||]
          Just t -> [hsx|<span class="text-muted">Edited at {dateTime t}</span>|]
        renderLikeButton =
          [hsx|
              <form method="POST" action={LikeReviewAction review.id}>
               <button type="submit" class="btn btn-link text-muted">Helpful</button>
              </form>
          |]

isCreatorOf :: Review -> Maybe User -> Bool
isCreatorOf review Nothing = False
isCreatorOf review (Just user) = review.userId == user.id
