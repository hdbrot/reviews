module Web.View.Books.Show where

import Web.View.Prelude

import Web.View.Reviews.Helper (newReviewOf, renderReviewForm, renderReview)

data ShowView = ShowView { review :: Review }

instance View ShowView where
  html ShowView { .. } = [hsx| {renderReview review} |]
