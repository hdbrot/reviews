module Web.View.Reviews.Edit where

import Web.View.Prelude

import Web.View.Reviews.Helper (renderReviewForm)

data EditView = EditView { review :: Review }


instance View EditView where
  html EditView { .. } =
    renderModal Modal { modalTitle = "Edit Review"
                      , modalCloseUrl = pathTo ShowBookAction { bookId = review.bookId }
                      , modalFooter = Nothing
                      , modalContent = [hsx| {renderReviewForm review} |]
                      }
