module Web.Controller.Sessions where

import qualified IHP.AuthSupport.Controller.Sessions as Sessions

import Web.Controller.Prelude
import Web.View.Sessions.New

instance Controller SessionsController where
  action NewSessionAction = do
    Sessions.newSessionAction @User

  action CreateSessionAction = do
    Sessions.createSessionAction @User

  action DeleteSessionAction = do
    Sessions.deleteSessionAction @User

instance Sessions.SessionsControllerConfig User
