module Web.Controller.Users where

import Web.Controller.Prelude
import Web.Controller.Sessions

import Web.Controller.Books

import Web.View.Users.New
import Web.View.Users.Edit

instance Controller UsersController where

  action NewUserAction = do
    let user = newRecord
    setModal NewView { .. }
    jumpToAction NewSessionAction

  action CreateUserAction = do
    let user = newRecord @User
    -- The value from the password confirmation input field.
    let passwordConfirmation = param @Text "passwordConfirmation"
    user
      |> fill @'["email", "passwordHash"]
      -- We ensure that the error message doesn't include
      -- the entered password.
      |> validateField #passwordHash (isEqual passwordConfirmation |> withCustomErrorMessage "Please provide matching passwords")
      |> validateField #passwordHash (nonEmpty |> withCustomErrorMessage "Please provide matching passwords.")
      |> validateField #email (isEmail |> withCustomErrorMessage "Please provide a valid email.")
      -- After this validation, since it's operation on the IO, we'll need to use >>=.
      |> validateIsUnique #email -- find correct type
      >>= ifValid \case
      Left user' -> do
        let user = set #passwordHash "" user'
        setModal NewView { .. }
        jumpToAction NewSessionAction
      Right user -> do
        hashed <- hashPassword user.passwordHash
        user <- user
          |> set #passwordHash hashed
          |> createRecord
        setSuccessMessage "You have registered successfully. You may now log in."
        redirectTo NewSessionAction

  action DeleteUserAction { userId } = do
    accessDeniedUnless (userId == currentUserId)

    user <- fetch userId
    deleteRecord user
    setSuccessMessage "User deleted"
    redirectTo WelcomeAction

buildUser user = user
  |> fill @'["email", "passwordHash", "failedLoginAttempts"]
