module Web.Controller.Reviews where

import Web.Controller.Prelude
import Web.View.Reviews.New
import Web.View.Reviews.Edit

import Web.Controller.Books ()

instance Controller ReviewsController where

  action NewReviewAction { bookId } = do
    ensureIsUser

    let maybeReview = Nothing
    setModal NewView { .. }
    jumpToAction ShowBookAction { bookId = bookId }

  action EditReviewAction { reviewId } = do
    ensureIsUser
    review <- fetch reviewId
    accessDeniedUnless (review.userId == currentUserId)

    setModal EditView { .. }
    jumpToAction ShowBookAction { bookId = review.bookId }

  action UpdateReviewAction { reviewId } = do
    ensureIsUser
    review <- fetch reviewId
    accessDeniedUnless (review.userId == currentUserId)

    let oldReview = review
    let oldTime = oldReview.editedAt
    review
      |> buildReview
      |> ifValid \case
      Left review -> do
        setModal EditView { .. }
        jumpToAction ShowBookAction { bookId = review.bookId }
      Right review -> do
        currentTime <- getCurrentTime
        review <- review
          |> set #editedAt (if review `simeq` oldReview then oldTime else pure currentTime)
          |> updateRecord
        redirectTo ShowBookAction { bookId = review.bookId }

  action CreateReviewAction = do
    ensureIsUser

    let review = newRecord @Review
    review
      |> buildReview
      |> fill @'["bookId", "createdAt"]
      |> set #userId currentUserId
      |> ifValid \case
      Left review -> do
        let bookId = review.bookId
        let maybeReview = Just review
        setModal NewView { .. }
        jumpToAction ShowBookAction { bookId = review.bookId }
      Right review -> do
        review <- review
          |> createRecord
        redirectTo ShowBookAction { bookId = review.bookId }

  action DeleteReviewAction { reviewId } = do
    review <- fetch reviewId
    accessDeniedUnless (review.userId == currentUserId)

    reviewIts <- fetch reviewId
      >>= fetchRelated #votes
    deleteRecords reviewIts.votes
    deleteRecord review
    redirectTo ShowBookAction { bookId = review.bookId }

  action LikeReviewAction { reviewId } = do
    ensureIsUser
    review <- fetch reviewId
    accessDeniedWhen (review.userId == currentUserId)

    maybeOldVote <- query @Vote
      |> filterWhere (#reviewId, reviewId)
      |> filterWhere (#userId, currentUserId)
      |> fetchOneOrNothing
    let oldScore = review.score
    case maybeOldVote of
      Just oldVote -> do review <- review
                           |> set #score (oldScore - 1)
                           |> updateRecord
                         vote <- oldVote
                           |> deleteRecord
                         redirectBack
      Nothing -> do review <- review
                      |> set #score (oldScore + 1)
                      |> updateRecord
                    vote <- newRecord @Vote
                      |> set #userId currentUserId
                      |> set #reviewId reviewId
                      |> createRecord
                    redirectBack

buildReview review = review
  |> fill @'["title", "rating", "body"]
  |> whiteSpaceToNothing #body
  |> validateField #title (validateMoreThanWhiteSpace |> withCustomErrorMessage "Please provide a title.")
  |> validateField #rating ((isInRange (1, 5)) |> withCustomErrorMessage "Please select a rating.")

review `simeq` review' =
  review.title == review'.title &&
  review.rating == review'.rating &&
  review.body == review'.body
