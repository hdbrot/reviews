module Web.Controller.Books where

import Web.Controller.Prelude
import Web.View.Books.Index
import Web.View.Books.New
import Web.View.Books.Edit
import Web.View.Books.Show

instance Controller BooksController where

  action BooksAction = do
    books <- query @Book
      |> fetch
      >>= collectionFetchRelated #reviews
    render IndexView { .. }

  action NewBookAction = do
    setModal NewView { book = newRecord }
    jumpToAction BooksAction

  action ShowBookAction { bookId } = do
    book <- fetchAndSortReviewsByParam bookId (paramOrNothing @Text "sort")
    render ShowView { .. }

  action EditBookAction { bookId } = do
    book <- fetch bookId
    setModal EditView { .. }
    jumpToAction ShowBookAction { bookId = bookId }

  action UpdateBookAction { bookId } = do
    book <- fetch bookId
    book
      |> buildBook
      |> ifValid \case
      Left book -> do
        setModal EditView { .. }
        jumpToAction ShowBookAction { bookId = bookId }
      Right book -> do
        book <- book |> updateRecord
        setSuccessMessage "Book pdated."
        redirectTo ShowBookAction { bookId = bookId }

  action CreateBookAction = do
    let book = newRecord @Book
    book
      |> buildBook
      |> ifValid \case
      Left book -> do
        setModal NewView { .. }
        jumpToAction BooksAction
      Right book -> do
        book <- book |> createRecord
        setSuccessMessage "Book created."
        redirectTo BooksAction

  action DeleteBookAction { bookId } = do
    book <- fetch bookId
    reviews <- fetch book.reviews
    if isEmpty reviews
      then do deleteRecord book
              setSuccessMessage "Book burned."
              redirectTo BooksAction
      else do setErrorMessage "Book already has reviews and thus can not be deleted."
              redirectTo ShowBookAction { bookId = bookId }

buildBook book = book
  |> fill @'["title", "author", "content"]
  |> whiteSpaceToNothing #author
  |> whiteSpaceToNothing #content
  |> validateField #title (validateMoreThanWhiteSpace |> withCustomErrorMessage "Please provide a title.")

fetchAndSortReviewsByParam bookId "dsc" = fetch bookId
  >>= pure . modify #reviews (orderByDesc #rating)
  >>= pure . modify #reviews (orderByDesc #score)
  >>= fetchRelated #reviews
fetchAndSortReviewsByParam bookId "asc" = fetch bookId
  >>= pure . modify #reviews (orderByAsc #rating)
  >>= pure . modify #reviews (orderByDesc #score)
  >>= fetchRelated #reviews
fetchAndSortReviewsByParam bookId _ = fetch bookId
  >>= pure . modify #reviews (orderByDesc #score)
  >>= fetchRelated #reviews
