-- This file is auto generated and will be overriden regulary. Please edit `Application/Schema.sql` to change the Types\n"
{-# LANGUAGE TypeSynonymInstances, FlexibleInstances, InstanceSigs, MultiParamTypeClasses, TypeFamilies, DataKinds, TypeOperators, UndecidableInstances, ConstraintKinds, StandaloneDeriving  #-}
{-# OPTIONS_GHC -Wno-unused-imports -Wno-dodgy-imports -Wno-unused-matches #-}
module Generated.ActualTypes (module Generated.ActualTypes, module Generated.Enums) where
import IHP.HaskellSupport
import IHP.ModelSupport
import CorePrelude hiding (id)
import Data.Time.Clock
import Data.Time.LocalTime
import qualified Data.Time.Calendar
import qualified Data.List as List
import qualified Data.ByteString as ByteString
import qualified Net.IP
import Database.PostgreSQL.Simple
import Database.PostgreSQL.Simple.FromRow
import Database.PostgreSQL.Simple.FromField hiding (Field, name)
import Database.PostgreSQL.Simple.ToField hiding (Field)
import qualified IHP.Controller.Param
import GHC.TypeLits
import Data.UUID (UUID)
import Data.Default
import qualified IHP.QueryBuilder as QueryBuilder
import qualified Data.Proxy
import GHC.Records
import Data.Data
import qualified Data.String.Conversions
import qualified Data.Text.Encoding
import qualified Data.Aeson
import Database.PostgreSQL.Simple.Types (Query (Query), Binary ( .. ))
import qualified Database.PostgreSQL.Simple.Types
import IHP.Job.Types
import IHP.Job.Queue ()
import qualified Control.DeepSeq as DeepSeq
import qualified Data.Dynamic
import Data.Scientific
import Generated.Enums
data Book' reviews = Book {id :: (Id' "books"), title :: Text, author :: (Maybe Text), content :: (Maybe Text), reviews :: reviews, meta :: MetaBag} deriving (Eq, Show)

type instance PrimaryKey "books" = UUID
type instance Include "reviews" (Book' reviews) = Book' [Review]

type Book = Book' (QueryBuilder.QueryBuilder "reviews")

type instance GetTableName (Book' _) = "books"
type instance GetModelByTableName "books" = Book

instance Default (Id' "books") where def = Id def

instance () => Table (Book' reviews) where
    tableName = "books"
    tableNameByteString = Data.Text.Encoding.encodeUtf8 "books"
    columnNames = ["id","title","author","content"]
    primaryKeyCondition Book { id } = [("id", toField id)]
    {-# INLINABLE primaryKeyCondition #-}



data Review' bookId userId votes = Review {id :: (Id' "reviews"), bookId :: bookId, userId :: userId, createdAt :: UTCTime, editedAt :: (Maybe UTCTime), title :: Text, rating :: Int, body :: (Maybe Text), score :: Int, votes :: votes, meta :: MetaBag} deriving (Eq, Show)

type instance PrimaryKey "reviews" = UUID
type instance Include "bookId" (Review' bookId userId votes) = Review' (GetModelById bookId) userId votes
type instance Include "userId" (Review' bookId userId votes) = Review' bookId (GetModelById userId) votes
type instance Include "votes" (Review' bookId userId votes) = Review' bookId userId [Vote]

type Review = Review' (Id' "books") (Id' "users")(QueryBuilder.QueryBuilder "votes")

type instance GetTableName (Review' _ _ _) = "reviews"
type instance GetModelByTableName "reviews" = Review

instance Default (Id' "reviews") where def = Id def

instance () => Table (Review' bookId userId votes) where
    tableName = "reviews"
    tableNameByteString = Data.Text.Encoding.encodeUtf8 "reviews"
    columnNames = ["id","book_id","user_id","created_at","edited_at","title","rating","body","score"]
    primaryKeyCondition Review { id } = [("id", toField id)]
    {-# INLINABLE primaryKeyCondition #-}



data User' reviews votes = User {id :: (Id' "users"), email :: Text, passwordHash :: Text, lockedAt :: (Maybe UTCTime), failedLoginAttempts :: Int, reviews :: reviews, votes :: votes, meta :: MetaBag} deriving (Eq, Show)

type instance PrimaryKey "users" = UUID
type instance Include "reviews" (User' reviews votes) = User' [Review] votes
type instance Include "votes" (User' reviews votes) = User' reviews [Vote]

type User = User' (QueryBuilder.QueryBuilder "reviews") (QueryBuilder.QueryBuilder "votes")

type instance GetTableName (User' _ _) = "users"
type instance GetModelByTableName "users" = User

instance Default (Id' "users") where def = Id def

instance () => Table (User' reviews votes) where
    tableName = "users"
    tableNameByteString = Data.Text.Encoding.encodeUtf8 "users"
    columnNames = ["id","email","password_hash","locked_at","failed_login_attempts"]
    primaryKeyCondition User { id } = [("id", toField id)]
    {-# INLINABLE primaryKeyCondition #-}



data Vote' userId reviewId = Vote {id :: (Id' "votes"), userId :: userId, reviewId :: reviewId, meta :: MetaBag} deriving (Eq, Show)

type instance PrimaryKey "votes" = UUID
type instance Include "userId" (Vote' userId reviewId) = Vote' (GetModelById userId) reviewId
type instance Include "reviewId" (Vote' userId reviewId) = Vote' userId (GetModelById reviewId)

type Vote = Vote' (Id' "users") (Id' "reviews")

type instance GetTableName (Vote' _ _) = "votes"
type instance GetModelByTableName "votes" = Vote

instance Default (Id' "votes") where def = Id def

instance () => Table (Vote' userId reviewId) where
    tableName = "votes"
    tableNameByteString = Data.Text.Encoding.encodeUtf8 "votes"
    columnNames = ["id","user_id","review_id"]
    primaryKeyCondition Vote { id } = [("id", toField id)]
    {-# INLINABLE primaryKeyCondition #-}


