-- This file is auto generated and will be overriden regulary. Please edit `Application/Schema.sql` to change the Types\n"
{-# LANGUAGE TypeSynonymInstances, FlexibleInstances, InstanceSigs, MultiParamTypeClasses, TypeFamilies, DataKinds, TypeOperators, UndecidableInstances, ConstraintKinds, StandaloneDeriving  #-}
{-# OPTIONS_GHC -Wno-unused-imports -Wno-dodgy-imports -Wno-unused-matches #-}
module Generated.Vote where
import IHP.HaskellSupport
import IHP.ModelSupport
import CorePrelude hiding (id)
import Data.Time.Clock
import Data.Time.LocalTime
import qualified Data.Time.Calendar
import qualified Data.List as List
import qualified Data.ByteString as ByteString
import qualified Net.IP
import Database.PostgreSQL.Simple
import Database.PostgreSQL.Simple.FromRow
import Database.PostgreSQL.Simple.FromField hiding (Field, name)
import Database.PostgreSQL.Simple.ToField hiding (Field)
import qualified IHP.Controller.Param
import GHC.TypeLits
import Data.UUID (UUID)
import Data.Default
import qualified IHP.QueryBuilder as QueryBuilder
import qualified Data.Proxy
import GHC.Records
import Data.Data
import qualified Data.String.Conversions
import qualified Data.Text.Encoding
import qualified Data.Aeson
import Database.PostgreSQL.Simple.Types (Query (Query), Binary ( .. ))
import qualified Database.PostgreSQL.Simple.Types
import IHP.Job.Types
import IHP.Job.Queue ()
import qualified Control.DeepSeq as DeepSeq
import qualified Data.Dynamic
import Data.Scientific
import Generated.ActualTypes
instance InputValue Vote where inputValue = IHP.ModelSupport.recordToInputValue


instance FromRow Vote where
    fromRow = do
        id <- field
        userId <- field
        reviewId <- field
        let theRecord = Vote id userId reviewId def { originalDatabaseRecord = Just (Data.Dynamic.toDyn theRecord) }
        pure theRecord


type instance GetModelName (Vote' _ _) = "Vote"

instance CanCreate Vote where
    create :: (?modelContext :: ModelContext) => Vote -> IO Vote
    create model = do
        List.head <$> sqlQuery "INSERT INTO votes (id, user_id, review_id) VALUES (?, ?, ?) RETURNING id, user_id, review_id" ((fieldWithDefault #id model, model.userId, model.reviewId))
    createMany [] = pure []
    createMany models = do
        sqlQuery (Query $ "INSERT INTO votes (id, user_id, review_id) VALUES " <> (ByteString.intercalate ", " (List.map (\_ -> "(?, ?, ?)") models)) <> " RETURNING id, user_id, review_id") (List.concat $ List.map (\model -> [toField (fieldWithDefault #id model), toField (model.userId), toField (model.reviewId)]) models)

instance CanUpdate Vote where
    updateRecord model = do
        List.head <$> sqlQuery "UPDATE votes SET id = ?, user_id = ?, review_id = ? WHERE id = ? RETURNING id, user_id, review_id" ((fieldWithUpdate #id model, fieldWithUpdate #userId model, fieldWithUpdate #reviewId model, model.id))

instance Record Vote where
    {-# INLINE newRecord #-}
    newRecord = Vote def def def  def


instance QueryBuilder.FilterPrimaryKey "votes" where
    filterWhereId id builder =
        builder |> QueryBuilder.filterWhere (#id, id)
    {-# INLINE filterWhereId #-}


instance SetField "id" (Vote' userId reviewId) (Id' "votes") where
    {-# INLINE setField #-}
    setField newValue (Vote id userId reviewId meta) =
        Vote newValue userId reviewId (meta { touchedFields = "id" : touchedFields meta })
instance SetField "userId" (Vote' userId reviewId) userId where
    {-# INLINE setField #-}
    setField newValue (Vote id userId reviewId meta) =
        Vote id newValue reviewId (meta { touchedFields = "userId" : touchedFields meta })
instance SetField "reviewId" (Vote' userId reviewId) reviewId where
    {-# INLINE setField #-}
    setField newValue (Vote id userId reviewId meta) =
        Vote id userId newValue (meta { touchedFields = "reviewId" : touchedFields meta })
instance SetField "meta" (Vote' userId reviewId) MetaBag where
    {-# INLINE setField #-}
    setField newValue (Vote id userId reviewId meta) =
        Vote id userId reviewId newValue
instance UpdateField "id" (Vote' userId reviewId) (Vote' userId reviewId) (Id' "votes") (Id' "votes") where
    {-# INLINE updateField #-}
    updateField newValue (Vote id userId reviewId meta) = Vote newValue userId reviewId (meta { touchedFields = "id" : touchedFields meta })
instance UpdateField "userId" (Vote' userId reviewId) (Vote' userId' reviewId) userId userId' where
    {-# INLINE updateField #-}
    updateField newValue (Vote id userId reviewId meta) = Vote id newValue reviewId (meta { touchedFields = "userId" : touchedFields meta })
instance UpdateField "reviewId" (Vote' userId reviewId) (Vote' userId reviewId') reviewId reviewId' where
    {-# INLINE updateField #-}
    updateField newValue (Vote id userId reviewId meta) = Vote id userId newValue (meta { touchedFields = "reviewId" : touchedFields meta })
instance UpdateField "meta" (Vote' userId reviewId) (Vote' userId reviewId) MetaBag MetaBag where
    {-# INLINE updateField #-}
    updateField newValue (Vote id userId reviewId meta) = Vote id userId reviewId newValue


