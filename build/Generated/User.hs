-- This file is auto generated and will be overriden regulary. Please edit `Application/Schema.sql` to change the Types\n"
{-# LANGUAGE TypeSynonymInstances, FlexibleInstances, InstanceSigs, MultiParamTypeClasses, TypeFamilies, DataKinds, TypeOperators, UndecidableInstances, ConstraintKinds, StandaloneDeriving  #-}
{-# OPTIONS_GHC -Wno-unused-imports -Wno-dodgy-imports -Wno-unused-matches #-}
module Generated.User where
import IHP.HaskellSupport
import IHP.ModelSupport
import CorePrelude hiding (id)
import Data.Time.Clock
import Data.Time.LocalTime
import qualified Data.Time.Calendar
import qualified Data.List as List
import qualified Data.ByteString as ByteString
import qualified Net.IP
import Database.PostgreSQL.Simple
import Database.PostgreSQL.Simple.FromRow
import Database.PostgreSQL.Simple.FromField hiding (Field, name)
import Database.PostgreSQL.Simple.ToField hiding (Field)
import qualified IHP.Controller.Param
import GHC.TypeLits
import Data.UUID (UUID)
import Data.Default
import qualified IHP.QueryBuilder as QueryBuilder
import qualified Data.Proxy
import GHC.Records
import Data.Data
import qualified Data.String.Conversions
import qualified Data.Text.Encoding
import qualified Data.Aeson
import Database.PostgreSQL.Simple.Types (Query (Query), Binary ( .. ))
import qualified Database.PostgreSQL.Simple.Types
import IHP.Job.Types
import IHP.Job.Queue ()
import qualified Control.DeepSeq as DeepSeq
import qualified Data.Dynamic
import Data.Scientific
import Generated.ActualTypes
instance InputValue User where inputValue = IHP.ModelSupport.recordToInputValue


instance FromRow User where
    fromRow = do
        id <- field
        email <- field
        passwordHash <- field
        lockedAt <- field
        failedLoginAttempts <- field
        let theRecord = User id email passwordHash lockedAt failedLoginAttempts (QueryBuilder.filterWhere (#userId, id) (QueryBuilder.query @Review)) (QueryBuilder.filterWhere (#userId, id) (QueryBuilder.query @Vote)) def { originalDatabaseRecord = Just (Data.Dynamic.toDyn theRecord) }
        pure theRecord


type instance GetModelName (User' _ _) = "User"

instance CanCreate User where
    create :: (?modelContext :: ModelContext) => User -> IO User
    create model = do
        List.head <$> sqlQuery "INSERT INTO users (id, email, password_hash, locked_at, failed_login_attempts) VALUES (?, ?, ?, ?, ?) RETURNING id, email, password_hash, locked_at, failed_login_attempts" ((fieldWithDefault #id model, model.email, model.passwordHash, fieldWithDefault #lockedAt model, fieldWithDefault #failedLoginAttempts model))
    createMany [] = pure []
    createMany models = do
        sqlQuery (Query $ "INSERT INTO users (id, email, password_hash, locked_at, failed_login_attempts) VALUES " <> (ByteString.intercalate ", " (List.map (\_ -> "(?, ?, ?, ?, ?)") models)) <> " RETURNING id, email, password_hash, locked_at, failed_login_attempts") (List.concat $ List.map (\model -> [toField (fieldWithDefault #id model), toField (model.email), toField (model.passwordHash), toField (fieldWithDefault #lockedAt model), toField (fieldWithDefault #failedLoginAttempts model)]) models)

instance CanUpdate User where
    updateRecord model = do
        List.head <$> sqlQuery "UPDATE users SET id = ?, email = ?, password_hash = ?, locked_at = ?, failed_login_attempts = ? WHERE id = ? RETURNING id, email, password_hash, locked_at, failed_login_attempts" ((fieldWithUpdate #id model, fieldWithUpdate #email model, fieldWithUpdate #passwordHash model, fieldWithUpdate #lockedAt model, fieldWithUpdate #failedLoginAttempts model, model.id))

instance Record User where
    {-# INLINE newRecord #-}
    newRecord = User def def def Nothing def def def def


instance QueryBuilder.FilterPrimaryKey "users" where
    filterWhereId id builder =
        builder |> QueryBuilder.filterWhere (#id, id)
    {-# INLINE filterWhereId #-}


instance SetField "id" (User' reviews votes) (Id' "users") where
    {-# INLINE setField #-}
    setField newValue (User id email passwordHash lockedAt failedLoginAttempts reviews votes meta) =
        User newValue email passwordHash lockedAt failedLoginAttempts reviews votes (meta { touchedFields = "id" : touchedFields meta })
instance SetField "email" (User' reviews votes) Text where
    {-# INLINE setField #-}
    setField newValue (User id email passwordHash lockedAt failedLoginAttempts reviews votes meta) =
        User id newValue passwordHash lockedAt failedLoginAttempts reviews votes (meta { touchedFields = "email" : touchedFields meta })
instance SetField "passwordHash" (User' reviews votes) Text where
    {-# INLINE setField #-}
    setField newValue (User id email passwordHash lockedAt failedLoginAttempts reviews votes meta) =
        User id email newValue lockedAt failedLoginAttempts reviews votes (meta { touchedFields = "passwordHash" : touchedFields meta })
instance SetField "lockedAt" (User' reviews votes) (Maybe UTCTime) where
    {-# INLINE setField #-}
    setField newValue (User id email passwordHash lockedAt failedLoginAttempts reviews votes meta) =
        User id email passwordHash newValue failedLoginAttempts reviews votes (meta { touchedFields = "lockedAt" : touchedFields meta })
instance SetField "failedLoginAttempts" (User' reviews votes) Int where
    {-# INLINE setField #-}
    setField newValue (User id email passwordHash lockedAt failedLoginAttempts reviews votes meta) =
        User id email passwordHash lockedAt newValue reviews votes (meta { touchedFields = "failedLoginAttempts" : touchedFields meta })
instance SetField "reviews" (User' reviews votes) reviews where
    {-# INLINE setField #-}
    setField newValue (User id email passwordHash lockedAt failedLoginAttempts reviews votes meta) =
        User id email passwordHash lockedAt failedLoginAttempts newValue votes (meta { touchedFields = "reviews" : touchedFields meta })
instance SetField "votes" (User' reviews votes) votes where
    {-# INLINE setField #-}
    setField newValue (User id email passwordHash lockedAt failedLoginAttempts reviews votes meta) =
        User id email passwordHash lockedAt failedLoginAttempts reviews newValue (meta { touchedFields = "votes" : touchedFields meta })
instance SetField "meta" (User' reviews votes) MetaBag where
    {-# INLINE setField #-}
    setField newValue (User id email passwordHash lockedAt failedLoginAttempts reviews votes meta) =
        User id email passwordHash lockedAt failedLoginAttempts reviews votes newValue
instance UpdateField "id" (User' reviews votes) (User' reviews votes) (Id' "users") (Id' "users") where
    {-# INLINE updateField #-}
    updateField newValue (User id email passwordHash lockedAt failedLoginAttempts reviews votes meta) = User newValue email passwordHash lockedAt failedLoginAttempts reviews votes (meta { touchedFields = "id" : touchedFields meta })
instance UpdateField "email" (User' reviews votes) (User' reviews votes) Text Text where
    {-# INLINE updateField #-}
    updateField newValue (User id email passwordHash lockedAt failedLoginAttempts reviews votes meta) = User id newValue passwordHash lockedAt failedLoginAttempts reviews votes (meta { touchedFields = "email" : touchedFields meta })
instance UpdateField "passwordHash" (User' reviews votes) (User' reviews votes) Text Text where
    {-# INLINE updateField #-}
    updateField newValue (User id email passwordHash lockedAt failedLoginAttempts reviews votes meta) = User id email newValue lockedAt failedLoginAttempts reviews votes (meta { touchedFields = "passwordHash" : touchedFields meta })
instance UpdateField "lockedAt" (User' reviews votes) (User' reviews votes) (Maybe UTCTime) (Maybe UTCTime) where
    {-# INLINE updateField #-}
    updateField newValue (User id email passwordHash lockedAt failedLoginAttempts reviews votes meta) = User id email passwordHash newValue failedLoginAttempts reviews votes (meta { touchedFields = "lockedAt" : touchedFields meta })
instance UpdateField "failedLoginAttempts" (User' reviews votes) (User' reviews votes) Int Int where
    {-# INLINE updateField #-}
    updateField newValue (User id email passwordHash lockedAt failedLoginAttempts reviews votes meta) = User id email passwordHash lockedAt newValue reviews votes (meta { touchedFields = "failedLoginAttempts" : touchedFields meta })
instance UpdateField "reviews" (User' reviews votes) (User' reviews' votes) reviews reviews' where
    {-# INLINE updateField #-}
    updateField newValue (User id email passwordHash lockedAt failedLoginAttempts reviews votes meta) = User id email passwordHash lockedAt failedLoginAttempts newValue votes (meta { touchedFields = "reviews" : touchedFields meta })
instance UpdateField "votes" (User' reviews votes) (User' reviews votes') votes votes' where
    {-# INLINE updateField #-}
    updateField newValue (User id email passwordHash lockedAt failedLoginAttempts reviews votes meta) = User id email passwordHash lockedAt failedLoginAttempts reviews newValue (meta { touchedFields = "votes" : touchedFields meta })
instance UpdateField "meta" (User' reviews votes) (User' reviews votes) MetaBag MetaBag where
    {-# INLINE updateField #-}
    updateField newValue (User id email passwordHash lockedAt failedLoginAttempts reviews votes meta) = User id email passwordHash lockedAt failedLoginAttempts reviews votes newValue


