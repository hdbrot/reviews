-- This file is auto generated and will be overriden regulary. Please edit `Application/Schema.sql` to change the Types\n"
{-# LANGUAGE TypeSynonymInstances, FlexibleInstances, InstanceSigs, MultiParamTypeClasses, TypeFamilies, DataKinds, TypeOperators, UndecidableInstances, ConstraintKinds, StandaloneDeriving  #-}
{-# OPTIONS_GHC -Wno-unused-imports -Wno-dodgy-imports -Wno-unused-matches #-}
module Generated.Review where
import IHP.HaskellSupport
import IHP.ModelSupport
import CorePrelude hiding (id)
import Data.Time.Clock
import Data.Time.LocalTime
import qualified Data.Time.Calendar
import qualified Data.List as List
import qualified Data.ByteString as ByteString
import qualified Net.IP
import Database.PostgreSQL.Simple
import Database.PostgreSQL.Simple.FromRow
import Database.PostgreSQL.Simple.FromField hiding (Field, name)
import Database.PostgreSQL.Simple.ToField hiding (Field)
import qualified IHP.Controller.Param
import GHC.TypeLits
import Data.UUID (UUID)
import Data.Default
import qualified IHP.QueryBuilder as QueryBuilder
import qualified Data.Proxy
import GHC.Records
import Data.Data
import qualified Data.String.Conversions
import qualified Data.Text.Encoding
import qualified Data.Aeson
import Database.PostgreSQL.Simple.Types (Query (Query), Binary ( .. ))
import qualified Database.PostgreSQL.Simple.Types
import IHP.Job.Types
import IHP.Job.Queue ()
import qualified Control.DeepSeq as DeepSeq
import qualified Data.Dynamic
import Data.Scientific
import Generated.ActualTypes
instance InputValue Review where inputValue = IHP.ModelSupport.recordToInputValue


instance FromRow Review where
    fromRow = do
        id <- field
        bookId <- field
        userId <- field
        createdAt <- field
        editedAt <- field
        title <- field
        rating <- field
        body <- field
        score <- field
        let theRecord = Review id bookId userId createdAt editedAt title rating body score (QueryBuilder.filterWhere (#reviewId, id) (QueryBuilder.query @Vote)) def { originalDatabaseRecord = Just (Data.Dynamic.toDyn theRecord) }
        pure theRecord


type instance GetModelName (Review' _ _ _) = "Review"

instance CanCreate Review where
    create :: (?modelContext :: ModelContext) => Review -> IO Review
    create model = do
        List.head <$> sqlQuery "INSERT INTO reviews (id, book_id, user_id, created_at, edited_at, title, rating, body, score) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?) RETURNING id, book_id, user_id, created_at, edited_at, title, rating, body, score" ((fieldWithDefault #id model, model.bookId, model.userId, fieldWithDefault #createdAt model, fieldWithDefault #editedAt model, model.title, model.rating, fieldWithDefault #body model) :. (Only (fieldWithDefault #score model)))
    createMany [] = pure []
    createMany models = do
        sqlQuery (Query $ "INSERT INTO reviews (id, book_id, user_id, created_at, edited_at, title, rating, body, score) VALUES " <> (ByteString.intercalate ", " (List.map (\_ -> "(?, ?, ?, ?, ?, ?, ?, ?, ?)") models)) <> " RETURNING id, book_id, user_id, created_at, edited_at, title, rating, body, score") (List.concat $ List.map (\model -> [toField (fieldWithDefault #id model), toField (model.bookId), toField (model.userId), toField (fieldWithDefault #createdAt model), toField (fieldWithDefault #editedAt model), toField (model.title), toField (model.rating), toField (fieldWithDefault #body model), toField (fieldWithDefault #score model)]) models)

instance CanUpdate Review where
    updateRecord model = do
        List.head <$> sqlQuery "UPDATE reviews SET id = ?, book_id = ?, user_id = ?, created_at = ?, edited_at = ?, title = ?, rating = ?, body = ?, score = ? WHERE id = ? RETURNING id, book_id, user_id, created_at, edited_at, title, rating, body, score" ((fieldWithUpdate #id model, fieldWithUpdate #bookId model, fieldWithUpdate #userId model, fieldWithUpdate #createdAt model, fieldWithUpdate #editedAt model, fieldWithUpdate #title model, fieldWithUpdate #rating model, fieldWithUpdate #body model) :. (fieldWithUpdate #score model, model.id))

instance Record Review where
    {-# INLINE newRecord #-}
    newRecord = Review def def def def Nothing def def Nothing def def def


instance QueryBuilder.FilterPrimaryKey "reviews" where
    filterWhereId id builder =
        builder |> QueryBuilder.filterWhere (#id, id)
    {-# INLINE filterWhereId #-}


instance SetField "id" (Review' bookId userId votes) (Id' "reviews") where
    {-# INLINE setField #-}
    setField newValue (Review id bookId userId createdAt editedAt title rating body score votes meta) =
        Review newValue bookId userId createdAt editedAt title rating body score votes (meta { touchedFields = "id" : touchedFields meta })
instance SetField "bookId" (Review' bookId userId votes) bookId where
    {-# INLINE setField #-}
    setField newValue (Review id bookId userId createdAt editedAt title rating body score votes meta) =
        Review id newValue userId createdAt editedAt title rating body score votes (meta { touchedFields = "bookId" : touchedFields meta })
instance SetField "userId" (Review' bookId userId votes) userId where
    {-# INLINE setField #-}
    setField newValue (Review id bookId userId createdAt editedAt title rating body score votes meta) =
        Review id bookId newValue createdAt editedAt title rating body score votes (meta { touchedFields = "userId" : touchedFields meta })
instance SetField "createdAt" (Review' bookId userId votes) UTCTime where
    {-# INLINE setField #-}
    setField newValue (Review id bookId userId createdAt editedAt title rating body score votes meta) =
        Review id bookId userId newValue editedAt title rating body score votes (meta { touchedFields = "createdAt" : touchedFields meta })
instance SetField "editedAt" (Review' bookId userId votes) (Maybe UTCTime) where
    {-# INLINE setField #-}
    setField newValue (Review id bookId userId createdAt editedAt title rating body score votes meta) =
        Review id bookId userId createdAt newValue title rating body score votes (meta { touchedFields = "editedAt" : touchedFields meta })
instance SetField "title" (Review' bookId userId votes) Text where
    {-# INLINE setField #-}
    setField newValue (Review id bookId userId createdAt editedAt title rating body score votes meta) =
        Review id bookId userId createdAt editedAt newValue rating body score votes (meta { touchedFields = "title" : touchedFields meta })
instance SetField "rating" (Review' bookId userId votes) Int where
    {-# INLINE setField #-}
    setField newValue (Review id bookId userId createdAt editedAt title rating body score votes meta) =
        Review id bookId userId createdAt editedAt title newValue body score votes (meta { touchedFields = "rating" : touchedFields meta })
instance SetField "body" (Review' bookId userId votes) (Maybe Text) where
    {-# INLINE setField #-}
    setField newValue (Review id bookId userId createdAt editedAt title rating body score votes meta) =
        Review id bookId userId createdAt editedAt title rating newValue score votes (meta { touchedFields = "body" : touchedFields meta })
instance SetField "score" (Review' bookId userId votes) Int where
    {-# INLINE setField #-}
    setField newValue (Review id bookId userId createdAt editedAt title rating body score votes meta) =
        Review id bookId userId createdAt editedAt title rating body newValue votes (meta { touchedFields = "score" : touchedFields meta })
instance SetField "votes" (Review' bookId userId votes) votes where
    {-# INLINE setField #-}
    setField newValue (Review id bookId userId createdAt editedAt title rating body score votes meta) =
        Review id bookId userId createdAt editedAt title rating body score newValue (meta { touchedFields = "votes" : touchedFields meta })
instance SetField "meta" (Review' bookId userId votes) MetaBag where
    {-# INLINE setField #-}
    setField newValue (Review id bookId userId createdAt editedAt title rating body score votes meta) =
        Review id bookId userId createdAt editedAt title rating body score votes newValue
instance UpdateField "id" (Review' bookId userId votes) (Review' bookId userId votes) (Id' "reviews") (Id' "reviews") where
    {-# INLINE updateField #-}
    updateField newValue (Review id bookId userId createdAt editedAt title rating body score votes meta) = Review newValue bookId userId createdAt editedAt title rating body score votes (meta { touchedFields = "id" : touchedFields meta })
instance UpdateField "bookId" (Review' bookId userId votes) (Review' bookId' userId votes) bookId bookId' where
    {-# INLINE updateField #-}
    updateField newValue (Review id bookId userId createdAt editedAt title rating body score votes meta) = Review id newValue userId createdAt editedAt title rating body score votes (meta { touchedFields = "bookId" : touchedFields meta })
instance UpdateField "userId" (Review' bookId userId votes) (Review' bookId userId' votes) userId userId' where
    {-# INLINE updateField #-}
    updateField newValue (Review id bookId userId createdAt editedAt title rating body score votes meta) = Review id bookId newValue createdAt editedAt title rating body score votes (meta { touchedFields = "userId" : touchedFields meta })
instance UpdateField "createdAt" (Review' bookId userId votes) (Review' bookId userId votes) UTCTime UTCTime where
    {-# INLINE updateField #-}
    updateField newValue (Review id bookId userId createdAt editedAt title rating body score votes meta) = Review id bookId userId newValue editedAt title rating body score votes (meta { touchedFields = "createdAt" : touchedFields meta })
instance UpdateField "editedAt" (Review' bookId userId votes) (Review' bookId userId votes) (Maybe UTCTime) (Maybe UTCTime) where
    {-# INLINE updateField #-}
    updateField newValue (Review id bookId userId createdAt editedAt title rating body score votes meta) = Review id bookId userId createdAt newValue title rating body score votes (meta { touchedFields = "editedAt" : touchedFields meta })
instance UpdateField "title" (Review' bookId userId votes) (Review' bookId userId votes) Text Text where
    {-# INLINE updateField #-}
    updateField newValue (Review id bookId userId createdAt editedAt title rating body score votes meta) = Review id bookId userId createdAt editedAt newValue rating body score votes (meta { touchedFields = "title" : touchedFields meta })
instance UpdateField "rating" (Review' bookId userId votes) (Review' bookId userId votes) Int Int where
    {-# INLINE updateField #-}
    updateField newValue (Review id bookId userId createdAt editedAt title rating body score votes meta) = Review id bookId userId createdAt editedAt title newValue body score votes (meta { touchedFields = "rating" : touchedFields meta })
instance UpdateField "body" (Review' bookId userId votes) (Review' bookId userId votes) (Maybe Text) (Maybe Text) where
    {-# INLINE updateField #-}
    updateField newValue (Review id bookId userId createdAt editedAt title rating body score votes meta) = Review id bookId userId createdAt editedAt title rating newValue score votes (meta { touchedFields = "body" : touchedFields meta })
instance UpdateField "score" (Review' bookId userId votes) (Review' bookId userId votes) Int Int where
    {-# INLINE updateField #-}
    updateField newValue (Review id bookId userId createdAt editedAt title rating body score votes meta) = Review id bookId userId createdAt editedAt title rating body newValue votes (meta { touchedFields = "score" : touchedFields meta })
instance UpdateField "votes" (Review' bookId userId votes) (Review' bookId userId votes') votes votes' where
    {-# INLINE updateField #-}
    updateField newValue (Review id bookId userId createdAt editedAt title rating body score votes meta) = Review id bookId userId createdAt editedAt title rating body score newValue (meta { touchedFields = "votes" : touchedFields meta })
instance UpdateField "meta" (Review' bookId userId votes) (Review' bookId userId votes) MetaBag MetaBag where
    {-# INLINE updateField #-}
    updateField newValue (Review id bookId userId createdAt editedAt title rating body score votes meta) = Review id bookId userId createdAt editedAt title rating body score votes newValue


