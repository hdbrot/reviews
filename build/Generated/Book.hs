-- This file is auto generated and will be overriden regulary. Please edit `Application/Schema.sql` to change the Types\n"
{-# LANGUAGE TypeSynonymInstances, FlexibleInstances, InstanceSigs, MultiParamTypeClasses, TypeFamilies, DataKinds, TypeOperators, UndecidableInstances, ConstraintKinds, StandaloneDeriving  #-}
{-# OPTIONS_GHC -Wno-unused-imports -Wno-dodgy-imports -Wno-unused-matches #-}
module Generated.Book where
import IHP.HaskellSupport
import IHP.ModelSupport
import CorePrelude hiding (id)
import Data.Time.Clock
import Data.Time.LocalTime
import qualified Data.Time.Calendar
import qualified Data.List as List
import qualified Data.ByteString as ByteString
import qualified Net.IP
import Database.PostgreSQL.Simple
import Database.PostgreSQL.Simple.FromRow
import Database.PostgreSQL.Simple.FromField hiding (Field, name)
import Database.PostgreSQL.Simple.ToField hiding (Field)
import qualified IHP.Controller.Param
import GHC.TypeLits
import Data.UUID (UUID)
import Data.Default
import qualified IHP.QueryBuilder as QueryBuilder
import qualified Data.Proxy
import GHC.Records
import Data.Data
import qualified Data.String.Conversions
import qualified Data.Text.Encoding
import qualified Data.Aeson
import Database.PostgreSQL.Simple.Types (Query (Query), Binary ( .. ))
import qualified Database.PostgreSQL.Simple.Types
import IHP.Job.Types
import IHP.Job.Queue ()
import qualified Control.DeepSeq as DeepSeq
import qualified Data.Dynamic
import Data.Scientific
import Generated.ActualTypes
instance InputValue Book where inputValue = IHP.ModelSupport.recordToInputValue


instance FromRow Book where
    fromRow = do
        id <- field
        title <- field
        author <- field
        content <- field
        let theRecord = Book id title author content (QueryBuilder.filterWhere (#bookId, id) (QueryBuilder.query @Review)) def { originalDatabaseRecord = Just (Data.Dynamic.toDyn theRecord) }
        pure theRecord


type instance GetModelName (Book' _) = "Book"

instance CanCreate Book where
    create :: (?modelContext :: ModelContext) => Book -> IO Book
    create model = do
        List.head <$> sqlQuery "INSERT INTO books (id, title, author, content) VALUES (?, ?, ?, ?) RETURNING id, title, author, content" ((fieldWithDefault #id model, model.title, fieldWithDefault #author model, fieldWithDefault #content model))
    createMany [] = pure []
    createMany models = do
        sqlQuery (Query $ "INSERT INTO books (id, title, author, content) VALUES " <> (ByteString.intercalate ", " (List.map (\_ -> "(?, ?, ?, ?)") models)) <> " RETURNING id, title, author, content") (List.concat $ List.map (\model -> [toField (fieldWithDefault #id model), toField (model.title), toField (fieldWithDefault #author model), toField (fieldWithDefault #content model)]) models)

instance CanUpdate Book where
    updateRecord model = do
        List.head <$> sqlQuery "UPDATE books SET id = ?, title = ?, author = ?, content = ? WHERE id = ? RETURNING id, title, author, content" ((fieldWithUpdate #id model, fieldWithUpdate #title model, fieldWithUpdate #author model, fieldWithUpdate #content model, model.id))

instance Record Book where
    {-# INLINE newRecord #-}
    newRecord = Book def def Nothing Nothing def def


instance QueryBuilder.FilterPrimaryKey "books" where
    filterWhereId id builder =
        builder |> QueryBuilder.filterWhere (#id, id)
    {-# INLINE filterWhereId #-}


instance SetField "id" (Book' reviews) (Id' "books") where
    {-# INLINE setField #-}
    setField newValue (Book id title author content reviews meta) =
        Book newValue title author content reviews (meta { touchedFields = "id" : touchedFields meta })
instance SetField "title" (Book' reviews) Text where
    {-# INLINE setField #-}
    setField newValue (Book id title author content reviews meta) =
        Book id newValue author content reviews (meta { touchedFields = "title" : touchedFields meta })
instance SetField "author" (Book' reviews) (Maybe Text) where
    {-# INLINE setField #-}
    setField newValue (Book id title author content reviews meta) =
        Book id title newValue content reviews (meta { touchedFields = "author" : touchedFields meta })
instance SetField "content" (Book' reviews) (Maybe Text) where
    {-# INLINE setField #-}
    setField newValue (Book id title author content reviews meta) =
        Book id title author newValue reviews (meta { touchedFields = "content" : touchedFields meta })
instance SetField "reviews" (Book' reviews) reviews where
    {-# INLINE setField #-}
    setField newValue (Book id title author content reviews meta) =
        Book id title author content newValue (meta { touchedFields = "reviews" : touchedFields meta })
instance SetField "meta" (Book' reviews) MetaBag where
    {-# INLINE setField #-}
    setField newValue (Book id title author content reviews meta) =
        Book id title author content reviews newValue
instance UpdateField "id" (Book' reviews) (Book' reviews) (Id' "books") (Id' "books") where
    {-# INLINE updateField #-}
    updateField newValue (Book id title author content reviews meta) = Book newValue title author content reviews (meta { touchedFields = "id" : touchedFields meta })
instance UpdateField "title" (Book' reviews) (Book' reviews) Text Text where
    {-# INLINE updateField #-}
    updateField newValue (Book id title author content reviews meta) = Book id newValue author content reviews (meta { touchedFields = "title" : touchedFields meta })
instance UpdateField "author" (Book' reviews) (Book' reviews) (Maybe Text) (Maybe Text) where
    {-# INLINE updateField #-}
    updateField newValue (Book id title author content reviews meta) = Book id title newValue content reviews (meta { touchedFields = "author" : touchedFields meta })
instance UpdateField "content" (Book' reviews) (Book' reviews) (Maybe Text) (Maybe Text) where
    {-# INLINE updateField #-}
    updateField newValue (Book id title author content reviews meta) = Book id title author newValue reviews (meta { touchedFields = "content" : touchedFields meta })
instance UpdateField "reviews" (Book' reviews) (Book' reviews') reviews reviews' where
    {-# INLINE updateField #-}
    updateField newValue (Book id title author content reviews meta) = Book id title author content newValue (meta { touchedFields = "reviews" : touchedFields meta })
instance UpdateField "meta" (Book' reviews) (Book' reviews) MetaBag MetaBag where
    {-# INLINE updateField #-}
    updateField newValue (Book id title author content reviews meta) = Book id title author content reviews newValue


